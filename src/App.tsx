import React, { useState } from "react";
import {Button, Table, Form } from 'react-bootstrap';
import { ITask, IFormTask } from "./interfaces/task.interface";

function App() {
  const [list, setList] = useState<ITask[]>([]);
  const [formTask, setFormTask] = useState<IFormTask>({});

  const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const {value, name} = event.target;

    setFormTask({
      ...formTask,
      [name]: value
    })
  }

  const createTask = () =>{
    if(!formTask.title || !formTask.description){
      alert("Campos obrigatório");
      return;
    }

    const newTask: ITask = {
      title: formTask.title,
      description: formTask.description
    }
    setList([...list, newTask])
  }

  return <div className="App">
    <Form>
      <Form.Group className="mb-3">
        <Form.Label>Título</Form.Label>
        <Form.Control type="text" name="title" onChange={handleChangeInput} value={formTask.title || ""} placeholder="Digite o título da tarefa" />
      </Form.Group>

      <Form.Group className="mb-3">
        <Form.Label>Descrição</Form.Label>
        <Form.Control type="text" onChange={handleChangeInput} name="description" value={formTask.description || ""} placeholder="Digite a descrição da tarefa" />
      </Form.Group>

      <Button variant="primary" type="button" onClick={createTask}>
        Cadastrar
      </Button>
    </Form>
    <section>
      {JSON.stringify(list)}
    </section>
  </div>;
}

export default App;
