export interface ITask {
  title: string;
  description: string;
}

export interface IFormTask{
    title?:string;
    description?:string;
}
